import React from 'react'
import { Alert, StyleSheet, Text, View,ActivityIndicator, ScrollView, Image, TouchableOpacity } from 'react-native';
import Swipeable from 'react-native-swipeable-row';
import axios from 'axios'
import _ from 'lodash'

class Item extends React.Component {


    constructor(props) {
        super(props)
    }

    componentDidMount() {
        axios.get('http://newitems.ipsupply.net/api/items/' + this.props.navigation.getParam('id'))
            .then((res) => {
                // console.log(res.data)
                this.setState({
                    item: res.data
                })
            })
            .catch((err) => console.log(err))
    }

    state = {
        id: '',
        item: {}
    }

    renderItem = () => {
        const { Item } = this.state.item
        const { ItemID, Title, Country, ConditionDisplayName, Location, ListingType, } = Item
        return (
            <View>
                <Text>ID: {ItemID}</Text>
                <Text>{Title}</Text>
 
                <Text>{Country}</Text>
                <Text>Condition: {ConditionDisplayName}</Text>

                <Text>{Location}</Text>

                <Text>Listing Type: {ListingType}</Text>
            </View>
        )
    }

    render() {
        const item = this.state.item
        if(Object.keys(item).length > 0) {
            return this.renderItem()
        } else {
            return (
            <View style={{flex: 1,  justifyContent: 'center'}}>
                <ActivityIndicator size="large" />
            </View>
            )
        }
    }
}

export default Item