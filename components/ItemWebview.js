import React from 'react'
import { WebView } from 'react-native'
class ItemWebview extends React.Component {
    constructor(props)
    {
        super(props)
    }
    render() {
        const { id } = this.props.navigation.state.params;

        let link = this.props.navigation.getParam('link') 
        return (
            <WebView
                
                source={{ uri: link }}
                javaScriptEnabled={true}
                style={{ flex: 1 }}
            />
        )
    }
}

export default ItemWebview