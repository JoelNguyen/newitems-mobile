import React, { Component } from 'react'
import {
    StyleSheet, Text, 
    View, 
    Button, ScrollView,
    Image, TouchableOpacity,
    ActionSheetIOS,  Linking, LinkingIOS,
    RefreshControl} from 'react-native';
import Pusher from 'pusher-js/react-native';
import axios from 'axios'
// import PushNotification from 'react-native-push-notification';
import env from 'react-native-config'
import Loading from './Loading';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    items: {

    },
    card: {
        flex: 1,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        padding: 10,
        alignItems: 'center',
    },
    buttonStyles: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20,
        backgroundColor: 'black',
    }
});


export default class Items extends Component {

    constructor(props) {
        super(props)
    }


    _isMounted = false;

    state = {
        items: [],
        products: [],
        swipeable: null,
        currentlyOpenSwipeable: null,
        url : '',
        showWebView: false,
        refreshing: false,
        loading: true,
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    blacklistItem = async (title) => {
        this._onLoad(true)

        await axios.post(env.API_HOST_PRODUCTION + `api/items/blacklist`, {data: title})
        .then(() => {
            this._onLoad(false)
            this._onRefresh()
        })
        .catch(() => this._onRefresh())
    }

    blacklistSeller = async (seller) => {
        this._onLoad(true)

        await axios.post(env.API_HOST_PRODUCTION + `api/seller/blacklist`, {data: seller})
        .then(() => {
            this._onLoad(false)
            this._onRefresh()
        })
        .catch(() => this._onRefresh())
    }


    deleteFromiPhone = async (id, index) => {
        this._isMounted = true
        const { items } = this.state
        items.splice(index, 1);
        if (this._isMounted) {
            this.setState({
                items: items
            })
        }
    }

    _onLoad = (status) => {
        this.setState({
            loading: status
        })
    }


    _onRefresh = () => {
        this.setState({refreshing: true});
        axios.get(env.API_HOST_PRODUCTION+'api/mobile/items')
        .then((res) => {
            this.setState({refreshing: false});
            let data = res.data.data;
            if (this._isMounted) {
                this.setState({
                    items: data
                })
            }
        })
        .catch((error) => {
            console.log(error)
        })
    }


    componentDidMount() {
        this._isMounted = true
      
        axios.get(env.API_HOST_PRODUCTION + 'api/mobile/items')
            .then((res) => {
                let data = res.data.data;
                if (this._isMounted) {
                    this.setState({
                        items: data,
                        loading: false
                    })
                }
            })
            .catch((error) => {
                console.log(error)
            })
    };

    onOpen = (event, gestureState, swipeable) => {
        const { currentlyOpenSwipeable } = this.state;
        if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
            currentlyOpenSwipeable.recenter();
        }
        this._isMounted = true
        if (this._isMounted) {
            this.setState({ currentlyOpenSwipeable: swipeable });
        }
    }

    onClose = () => {
        this.setState({ currentlyOpenSwipeable: null })

    }

    handleUserBeganScrollingParentView(id) {
        const { currentlyOpenSwipeable } = this.state;
        if (currentlyOpenSwipeable) {
            currentlyOpenSwipeable.recenter();
        }
    }

    blacklist_category(category, title) {
        axios.post(env.API_HOST_PRODUCTION + 'api/category/blacklist', {category, title})
        .then(() => {
            this._onLoad(false)
            this._onRefresh()
        })
        .catch(() => this._onRefresh())
    }


    componentWillMount() {
        this._isMounted = true
        var pusher = new Pusher(env.PUSHER_KEY, {
            cluster: 'ap4',
            forceTLS: true
        });

        const channel = pusher.subscribe('my-channel')

        channel.bind('App\\Events\\MessagePushed', (data) => {
            if (data.data && data.data.id) {
                this._onRefresh();
            
            }
        })
    }
    
    renderContentWebView() {
        return(
            <WebView 
                source={{ uri: this.state.url }}
                scalesPageToFit
                javaScriptEnabled
                style={{ flex: 1 }}
                
            />
        )
    }


    render() {
        return (

            <ScrollView 
            refreshControl={
                <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
                />
            }
            style={styles.container}>
            {/* { this.state.showWebView ? this.renderContentWebView() : <Text></Text> } */}
                <Button
                    onPress={() =>{                   
                        axios.post(env.API_HOST_PRODUCTION + `api/set-seen-all`);
                        this._onRefresh();

                    }}
                    title={'Set seen all item'}
                />
{this.state.loading ?  <Loading /> : <Text></Text>}
                {
                    this.state.items.length > 0 ?
                    this.state.items.map((item, index) => {
                        this._isMounted = true
                        return <TouchableOpacity
                                key={item.id}
                                onPress={() => {
                                        // navigate to show detail
                                    ActionSheetIOS.showActionSheetWithOptions({
                                        options: ['Show Details', `Blacklist seller: ${item.seller}`, `Blacklist category: ${item.category}`,'Cancel'],
                                        destructiveButtonIndex: 4,
                                        cancelButtonIndex: 3,
                                        title: item.title
                                        
                                    }, (buttonIndex) => {
                                        // when user press "Show Details Button"
                                        if(buttonIndex === 0) {
                                            var link = `https://www.ebay.com/itm/${item.id}`
                                            this.props.navigation.push('eBayDetail', { link: link })
                                            // Linking.openURL(`https://www.ebay.com.au/itm/${item.id}`);

                                            // this.setState({ showWebView: true})

                                        }
                    

                                        // when user press "Blacklist Seller"
                                        if(buttonIndex === 1) {
                                            this.blacklistSeller(item.seller)
                            
                                        }

                                        // when user press "Blacklist Category"
                                        if(buttonIndex === 2) {
                                            // this.blacklistSeller(item.seller)
                                            this.blacklist_category(item.category, item.title)
                                        }

                              

                                    })

                                }}
                            >

                                <View style={styles.card}>
                                    <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                                        <Image
                                            style={{ width: 150, height: 100, marginRight: 10 }}
                                            source={{ uri: item.picture }}
                                        />
                                        <View style={{ flex: 1, flexWrap: 'wrap' }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 19 }}>${item.price}</Text>
                                            <Text style={{ color: 'grey', marginTop: 6 }}>${item.price} Shipping</Text>
                                            <Text style={{ fontWeight: 'bold'}}>Category: {item.category}</Text>
                                        </View>
                                        <View style={{ flex: 1, flexWrap: 'wrap', alignItems: 'flex-end' }}>
                                            <Text style={{ fontWeight: 'bold' }}>{item.item_condition}</Text>
                                            {item.has_seen === 0 ? <Text style={{ fontWeight: 'bold', color: 'green' }}>New Item</Text> : <Text></Text>}
                                        </View>

                                    </View>

                                    <View style={{ flex: 1, flexWrap: 'wrap', alignItems: 'flex-start' }}>
                                        <Text>{item.title}</Text>
                                        <Text style={{ fontWeight: 'bold' }}>Seller: {item.seller}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            {/* </View> */}
                    })
                    // otherise return nothing
                : <Text></Text>
                }
            </ScrollView>
        );
    }
}
