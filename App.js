import React from 'react';
import Items from './components/Items';

import { createStackNavigator, createAppContainer } from 'react-navigation'
import Item from './components/Item';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import Storage from './models/Storage';
import ItemWebview from './components/ItemWebview';



const prefix = 'ipsupplyNewitem://ipsupplyNewitem/';

const RootStack = createStackNavigator({
  Home: {
    screen: Items,
    navigationOptions: () => ({
      title: 'Items'
    })
  },
  eBayDetail: {
    screen: ItemWebview,
    path: 'ebay/:id',
  },
  Item:
  {
    screen: Item,
    navigationOptions: ({ navigation }) => ({
      title: navigation.getParam('title')
    })
  }
}, {
  initialRouteName: 'Home'
})

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {

  constructor(properties) {
    super(properties);
    OneSignal.init("10362938-c280-43b7-ac1f-86cecb194363", { kOSSettingsKeyInFocusDisplayOption: 2, kOSSettingsKeyInAppLaunchURL: false, kOSSSettingsKeyPromptBeforeOpeningPushURL: false });

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.configure(); 	// triggers the ids event

  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);

    // OneSignal.inFocusDisplaying(0)
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
    const { data } = notification.payload.additionalData

  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
  }

  componentDidMount() {
  }



  render() {
    return (
      <React.Fragment>

        <AppContainer uriPrefix={prefix} />


      </React.Fragment>
    )
  }
};
