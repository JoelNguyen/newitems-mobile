//
//  NotificationService.h
//  OneSignalNotificationExtension
//
//  Created by Nguyễn Ngọc Anh on 16/7/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
