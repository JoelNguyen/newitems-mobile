import {AsyncStorage} from 'react-native';

class Storage {
    constructor(){
    }

    storeData = async (key, JSONdata) => {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(JSONdata));
          } catch (error) {
            console.log(error)
          }
    }

    retrieveData = (key) => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(key)
            .then((data) => {
                resolve(JSON.parse(data))
            })
            .catch((err) => {
                reject(err)
            })
        })
    }

    removeAllData = async (key) => {
        try {
            await AsyncStorage.removeItem(key)
        } catch(error) {
            console.log(error)
        }
    }

    
}

export default Storage